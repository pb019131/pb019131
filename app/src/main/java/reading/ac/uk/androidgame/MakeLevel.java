package reading.ac.uk.androidgame;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MakeLevel extends AppCompatActivity {

    //private String fileString = "player;100;100;player;" + "\n";
    private String fileString = "player;100;100;player;";
    private Button buttonWall;
    private Button buttonEnemy;
    private Button buttonCollectable;
    private Button buttonDestroyable;
    private Button buttonLaunch;
    private boolean listening = false;
    private Thread thread;

    private MotionEvent motionEvent;

    //These objects will be used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private Context context;
    private ConstraintLayout container;
    private View v;
    private MockView mock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_level);

        //mock

        buttonWall = findViewById(R.id.buttonWall);
        buttonEnemy = findViewById(R.id.buttonEnemy);
        buttonCollectable = findViewById(R.id.buttonCollectable);
        buttonDestroyable = findViewById(R.id.buttonDestroyable);
        buttonLaunch = findViewById(R.id.buttonLaunch);

        //unsuccessful implementation of drawing marks where objects have been placed

        this.context = getApplicationContext();

        Display display = getWindowManager().getDefaultDisplay();
        Point resolution = new Point();
        display.getSize(resolution);

        //initializing drawing objects
        //mock = new MockView(context, resolution.x, resolution.y);
        paint = new Paint();
        paint.setColor(Color.RED);
        canvas = new Canvas();
        //layoutMakeLevel
/*
        LayoutInflater mLayoutInflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View makeLevelView = mLayoutInflater.inflate(R.layout.activity_make_level, (ViewGroup) findViewById(R.id.layoutMakeLevel));
        //mock = findViewById(R.id.drawView);

        container = new ConstraintLayout(this);
        container.addView(mock);
        container.addView(makeLevelView);


        setContentView(container);*/
    }

    /**
     * Method to launch and load the created level
     * @param view to identify the root of button
     */
    public void launch(View view){
        //final line
        fileString = fileString + "win;445;1700;win;";

        //makeFile();

        Intent launchGame = new Intent(this, Game.class);
        //add extra message of which level to load
        String levelFile = fileString;
        launchGame.putExtra("LEVEL_FILE", levelFile);
        //start
        startActivity(launchGame);
    }

    /**
     * Method stub for writing the level to a file
     */
    public void makeFile(){

    }

    /**
     * Method to add the enemy game object into the level string
     * @param view to identify the root of button
     */
    public void addEnemy(View view){
        final View buttons = view;
        disableButtons(buttons);

        thread=  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){
                        wait(3000);
                    }
                }
                catch(InterruptedException ex){
                    ex.printStackTrace();
                }

                if(motionEvent != null){
                    fileString = fileString + "enemy" + ";" + ((int)motionEvent.getX()) + ";" + ((int)motionEvent.getY()) + ";" +"enemy1" + ";";// + "\n";
                    //v.draw();
                    //mock.draw(motionEvent.getX(), motionEvent.getY());//canvas.drawCircle(motionEvent.getX(), motionEvent.getY(), 30f, paint);
                    enableButtons(buttons);
                    System.out.println(fileString);
                }
            }
        };
        thread.start();
    }

    /**
     * Method to add the solid game object into the level string
     * @param view to identify the root of button
     */
    public void addWall(View view){
        final View buttons = view;
        disableButtons(buttons);

        thread=  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){
                        wait(3000);
                    }
                }
                catch(InterruptedException ex){
                    ex.printStackTrace();
                }

                if(motionEvent != null){
                    fileString = fileString + "solid" + ";" + ((int)motionEvent.getX()) + ";" + ((int)motionEvent.getY()) + ";" +"wall_user" + ";";// + "\n";
                    //v.draw();
                    //mock.draw(motionEvent.getX(), motionEvent.getY());//canvas.drawCircle(motionEvent.getX(), motionEvent.getY(), 30f, paint);
                    enableButtons(buttons);
                    System.out.println(fileString);
                }
            }
        };
        thread.start();
    }

    /**
     * Method to add the collectable game object into the level string
     * @param view to identify the root of button
     */
    public void addCollectable(View view){
        final View buttons = view;
        disableButtons(buttons);

        thread=  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){
                        wait(3000);
                    }
                }
                catch(InterruptedException ex){
                    ex.printStackTrace();
                }

                if(motionEvent != null){
                    fileString = fileString + "collectable" + ";" + ((int)motionEvent.getX()) + ";" + ((int)motionEvent.getY()) + ";" +"gem_yellow" + ";";// + "\n";
                    //v.draw();
                    //mock.draw(motionEvent.getX(), motionEvent.getY());//canvas.drawCircle(motionEvent.getX(), motionEvent.getY(), 30f, paint);
                    enableButtons(buttons);
                    System.out.println(fileString);
                }
            }
        };
        thread.start();
    }

    /**
     * Method to add the destroyable game object into the level string
     * @param view to identify the root of button
     */
    public void addDestroyable(View view){
        final View buttons = view;
        disableButtons(buttons);

        thread=  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){
                        wait(3000);
                    }
                }
                catch(InterruptedException ex){
                    ex.printStackTrace();
                }

                if(motionEvent != null){
                    fileString = fileString + "destroyable" + ";" + ((int)motionEvent.getX()) + ";" + ((int)motionEvent.getY()) + ";" +"art" + ";";// + "\n";
                    //v.draw();
                    //mock.draw(motionEvent.getX(), motionEvent.getY());//canvas.drawCircle(motionEvent.getX(), motionEvent.getY(), 30f, paint);
                    enableButtons(buttons);
                    System.out.println(fileString);
                }
            }
        };
        thread.start();
    }

    /**
     * Method to disable clicking on the buttons when adding a game object to level
     * was supposed to also hide the buttons
     * @param view to identify the root of button
     */
    private void disableButtons(View view) {
        buttonWall.setClickable(false);
        buttonEnemy.setClickable(false);
        buttonCollectable.setClickable(false);
        buttonDestroyable.setClickable(false);
        buttonLaunch.setClickable(false);

     /*   buttonWall.setVisibility(view.INVISIBLE);
        buttonEnemy.setVisibility(view.INVISIBLE);
        buttonCollectable.setVisibility(view.INVISIBLE);
        buttonDestroyable.setVisibility(view.INVISIBLE);
        buttonLaunch.setVisibility(view.INVISIBLE);*/

        listening = true;
    }

    /**
     * Method to enable clicking on the buttons when adding a game object to level
     * was supposed to also show the buttons
     * @param view to identify the root of button
     */
    private void enableButtons(View view) {
        buttonWall.setClickable(true);
        buttonEnemy.setClickable(true);
        buttonCollectable.setClickable(true);
        buttonDestroyable.setClickable(true);
        buttonLaunch.setClickable(true);

     /*   buttonWall.setVisibility(view.VISIBLE);
        buttonEnemy.setVisibility(view.VISIBLE);
        buttonCollectable.setVisibility(view.VISIBLE);
        buttonDestroyable.setVisibility(view.VISIBLE);
        buttonLaunch.setVisibility(view.VISIBLE);*/
    }

    /**
     * Method listen for touches after choosing to add a game object
     * @param motionEvent Android generated motion event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                listening = false;
                this.motionEvent = null;
                break;
            case MotionEvent.ACTION_DOWN:
                if(listening) {
                    this.motionEvent = motionEvent;
                    synchronized(thread){
                        thread.notifyAll();
                    }
                }
                break;
        }
        return true;
    }
}
