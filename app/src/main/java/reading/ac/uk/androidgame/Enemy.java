package reading.ac.uk.androidgame;

import android.content.Context;

import static java.lang.Math.atan2;
import static reading.ac.uk.androidgame.Item.itemRole.ENEMY;

public class Enemy extends Character {

    /**
     * Enemy character constructor
     * @param context - resource context;
     * @param xRes - x max value from resolution;
     * @param yRes - y max value from resolution;
     * @param x - location on x axis;
     * @param y - location on y axis;
     * @param bitmapName - bitmap name in /drawable/;
     */
    public Enemy(Context context, int xRes, int yRes, int x, int y, String bitmapName)
    {
        super(context, xRes, yRes, x, y, bitmapName);
        super.setItemRole(ENEMY);

        speed = 1.5;
        moving = true;
        angle = 0;
        momentum = momentumDefault;
    }

    /**
     * MOvement method almost identical to Player, but less complex since there is no laser;
     * @param newX - x coordinate to move to;
     * @param newY - y coordinate to move to;
     */
    @Override
    protected void move(float newX, float newY) {

        //normalize angle before calculation
        normalizeAngle();

        //adjust the momentum
        adjustMomentum();

        //distance to cover and angle of
        double xDist, yDist;

        xDist = (x - newX) * (-1);
        yDist = (y - newY) * (-1);
        angle = atan2(yDist, xDist);

        //save old coordinates for collision adjustment
        lastCoordinates = new float[] {x, y};

        //commit to new coordinates
        x = (float) (x + (speed * Math.cos(angle)) * momentum);
        y = (float) (y + (speed * Math.sin(angle)) * momentum);

        checkWall();

        //Adding the top, left, bottom and right to the rect object
        collisionRect.left = x;
        collisionRect.top = y;
        collisionRect.right = x + xSize;
        collisionRect.bottom = y + ySize;
    }

}
