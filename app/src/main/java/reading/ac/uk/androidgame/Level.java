package reading.ac.uk.androidgame;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class Level {

    //declaring variables
    private int xLevel, yLevel;
    private ArrayList<Item> itemList = new ArrayList<Item>();
    private ArrayList<Enemy> enemyList = new ArrayList<Enemy>();
    private int score = 0;

    //Player object
    private Character player;

    //drawing objects
    private Bitmap background;
    private MotionEvent mEvent;
    private GameView gameView;
    private Context levelContext;
    //a handle to the application's resources
    private Resources resources;


    /**
     * Class Level constructor;
     * Loads and configures the level file with it's data structure and prepares level for running;
     * @param context - to identify where the resources are located;
     * @param xRes - setting a X axis boundry based on resolution;
     * @param yRes - setting a Y axis boundry based on resolution;
     * @param gView - a GameView object to link the classes and allow calling higher up methods;
     * @throws Exception - if file not found
     */
    public Level(Context context, int xRes, int yRes, GameView gView) throws Exception{
        xLevel = xRes;
        yLevel = yRes;
        gameView = gView;
        levelContext = context;
        //check if the level file reference isn't null
        if(gameView.getLevelFile() != null){
            String levelFile = gameView.getLevelFile();
            //if file is not null attempt regular level loading
            try{
                loadLevel(levelFile);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        //if the file reference is null the game is using user made level
        else{
            String userLevel = gameView.getUserLevelFile();
            loadUserLevel(userLevel);
        }
    }

    /**
     * Method the moves the player based on touch coordinate values;
     * It also iterates through itemsList and enemyList ArrayLists to scan for collisions;
     * When a collision is found the role of item is checked and actions specific to roles carried out;
     */
    public void movePlayer() {
        if (player.isMoving() || player.getMomentum() != 0) {
            if (mEvent != null) {
                player.move(mEvent.getX(), mEvent.getY());

                int i = 0;
                while (i < itemList.size()) {
                    if (player != null && itemList.get(i) != null) {
                        if (player.checkCollision(itemList.get(i).getCollisionRect())) {
                            switch (itemList.get(i).getItemRole()) {

                                case "SOLID":
                                    player.adjustHit(itemList.get(i));
                                    break;

                                case "DESTROYABLE":
                                    editScore(((Destroyable) itemList.get(i)).getScorePenalty());
                                    itemList.remove(i);
                                    break;

                                case "ENEMY":
                                    gameOver();
                                    break;

                                case "COLLECTABLE":
                                    editScore(((Collectable) itemList.get(i)).getScoreBonus());
                                    itemList.remove(i);
                                    break;

                                case "WIN":
                                    levelComplete();
                                    break;
                            }
                        }
                    }
                    i++;
                }
            }
            //if trying to move but momentum 0, resets the momentum to default
        } else {
            player.resetMomentum();
        }
    }

    /**
     * Same method as movePlayer() but for enemies; Gets enemy objects from the arraylist one by one
     * and checks against all regular items in itemsList and then against other enemies in the arraylist;
     * Corrects the hits based on retrieved item role;
     */
    public void moveEnemies() {
        //temp variables
        //enemy object
        Enemy enemy;
        //object checked against
        Item temp;

        for (int i = 0; i < enemyList.size(); i++) {
            enemy = enemyList.get(i);
            enemy.move(player.getX(), player.getY());

            //checking against player
            if (enemy.checkCollision(player.getCollisionRect())) {
                gameOver();
                enemy.adjustHit(player);
                break;
            }

            //checking against objects
            for (int j = 0; j < itemList.size(); j++) {
                temp = itemList.get(j);
                if (enemy.checkCollision(temp.getCollisionRect())) {
                    switch (temp.getItemRole()) {

                        case "SOLID":
                            enemy.adjustHit(temp);
                            break;
                        case "ENEMY":
                            enemy.adjustHit(temp);
                            break;
                        case "DESTROYABLE":
                            enemy.adjustHit(temp);
                            break;
                        case "PLAYER":
                            gameOver();
                            break;
                    }
                }
            }
            //checking against other enemies
            for (int j = 0; j < enemyList.size(); j++) {
                if (i != j) {
                    temp = enemyList.get(j);
                    if (enemy.checkCollision(temp.getCollisionRect())) {
                        switch (temp.getItemRole()) {

                            case "SOLID":
                                enemy.adjustHit(temp);
                                break;
                            case "ENEMY":
                                enemy.adjustHit(temp);
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Method to draw all elements of the level; From background to items, enemies and player;
     * @param canvas - current canvas visible in View;
     * @param paint - drawing setting preset;
     */
    public void drawAll(Canvas canvas, Paint paint) {
        //drawing a background picture
        canvas.drawBitmap(background, 0, 0, paint);

        int i = 0;
        while (i < itemList.size()) {
            itemList.get(i).draw(canvas, paint);
            i++;
        }
        //Drawing the player
        player.draw(canvas, paint);

        i = 0;
        while (i < enemyList.size()) {
            enemyList.get(i).draw(canvas, paint);
            i++;
        }
    }

    /**
     * Method to call the pop-up AlertDialog in Game though GameView pop-up forward method
     */
    private void gameOver() {
        player.setMoving(false);
        gameView.showPopup("game over");
    }

    /**
     * Method to call the pop-up AlertDialog in Game though GameView pop-up forward method
     */
    private void levelComplete() {
        player.setMoving(false);
        calculateTotalScore();
        gameView.showPopup("win");
    }

    /**
     * Method to calculate the total score of the level; Performed as the level is completed;
     * Takes in score and gameTime and calculates the score based on the formula; The lowest score is 0;
     */
    private void calculateTotalScore() {
        long gameTime = gameView.getGameTime();
        int gameTimeSec = (int) (TimeUnit.MILLISECONDS.toSeconds(gameTime));
        //Total Score with time taken deduced as portion of points
        score = (score - ((score * gameTimeSec) / 100));

        if (score < 0) {
            score = 0;
        }
    }

    public int getScore() {
        return score;
    }

    /**
     * Method to set player movement values based on touch input from GameView class;
     * Prepares the objects for movePlayer() method;
     * @param state - state of the touch event; false - released; true - pressed;
     * @param mEvent - associated MotionEvent object to retrieve the coordinates in move methods;
     */
    public void handleTouch(boolean state, MotionEvent mEvent) {
        if (!state) {
            //if released stop moving and reset momentum
            player.setMoving(false);
            this.mEvent = mEvent;
            player.resetMomentum();
        }
        if (state) {
            //if touch action is happening set moving and fix momentum
            if (player.getMomentum() == 0) {
                player.resetMomentum();
            }
            player.setMoving(true);
            this.mEvent = mEvent;
        }
    }

    /**
     * Simple method to edit the score based on bonuses and penalties from collectables and destroyable items;
     * @param change - amount of points to add or deduct;
     */
    private void editScore(int change) {
        score = score + change;
    }

    /**
     * Generalised method of adding items into the level; Used for loading level file
     * and matching to the correct constructors and adding to respective ArrayLists;
     * @param type - String type of object to add, same as item role;
     * @param x - x coordinate of object;
     * @param y - y coordinate of object;
     * @param bitmapName - object' bitmap name in /drawable/ folder;
     */
    private void addItem(String type, int x, int y, String bitmapName) {

        if (type.equalsIgnoreCase("player")) {
            player = new Character(levelContext, xLevel, yLevel, x, y, bitmapName);
            return;
        }
        if (type.equalsIgnoreCase("enemy")) {
            enemyList.add(new Enemy(levelContext, xLevel, yLevel, x, y, bitmapName));
            return;
        }
        if (type.equalsIgnoreCase("solid")) {
            itemList.add(new Solid(levelContext, xLevel, yLevel, x, y, bitmapName));
            return;
        }
        if (type.equalsIgnoreCase("collectable")) {
            itemList.add(new Collectable(levelContext, xLevel, yLevel, x, y, bitmapName));
            return;
        }
        if (type.equalsIgnoreCase("destroyable")) {
            itemList.add(new Destroyable(levelContext, xLevel, yLevel, x, y, bitmapName));
            return;
        }
        if (type.equalsIgnoreCase("win")) {
            itemList.add(new Win(levelContext, xLevel, yLevel, x, y, bitmapName));
            return;
        }
    }

    /**
     * Additional method for loading, because user and regular levels have different procedures;
     * Loads user created level from a string;
     * @param userFile - the user level file; gets processed and loaded ;
     */
    public void loadUserLevel(String userFile){
        itemList.clear();
        enemyList.clear();
        score = 0;

        background = BitmapFactory.decodeResource(levelContext.getResources(), R.drawable.background);

        String[] fileValues = userFile.split(";");

        int i = 0;
        while (i < fileValues.length) {
            //read and add items
            addItem(fileValues[i], Integer.parseInt(fileValues[i + 1]), Integer.parseInt(fileValues[i + 2]), fileValues[i + 3]);
            i = i + 4;
        }
    }

    /**
     * Main method for loading level files; Loads level files from /raw/;
     * @param levelFile - the level file name to be searched in /raw/;
     * @exception Exception - file not available;
     */
    public void loadLevel(String levelFile) throws Exception{
        itemList.clear();
        enemyList.clear();
        score = 0;

        background = BitmapFactory.decodeResource(levelContext.getResources(), R.drawable.background);

        //get the application's resources
        resources = levelContext.getResources();

        //Create a InputStream to read the file into
        InputStream iS;

        System.out.println("############################");
        System.out.println("//" + levelFile +"///");

        int rawID = 0;

        try{
        rawID = resources.getIdentifier(levelFile, "raw", "reading.ac.uk.androidgame");
        }
        catch(Exception e){
            e.printStackTrace();
            gameOver();
        }

        //get the file as a stream
        iS = resources.openRawResource(rawID);

        String fileString = convertStreamToString(iS);

        String[] fileValues = fileString.split(";");

        int i = 0;
        while (i < fileValues.length) {
            //read and add items
            addItem(fileValues[i], Integer.parseInt(fileValues[i + 1]), Integer.parseInt(fileValues[i + 2]), fileValues[i + 3]);
            i = i + 4;
        }
    }

    /**
     * Method to convert input Stream into String and remove unwanted characters
     * @param iS - inputStream that is going to be converted into array
     * @return cleaned single line levelFile String;
     */
    private static String convertStreamToString (java.io.InputStream iS){
        String fileString = "";
        //get the whole string and cut up
        java.util.Scanner s = new java.util.Scanner(iS).useDelimiter("\r\n\\s");
        while(s.hasNext()){
            fileString = fileString + s.nextLine();
        }
        s.close();
        return fileString;
    }
}
