package reading.ac.uk.androidgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public abstract class Item {

    //declaring common variables
    protected float x, y; //coordinates
    protected float xSize, ySize; //size in 2D
    protected float xMax, yMax; //screen resolution boundaries
    protected String bitmapName;
    protected Bitmap bitmap;
    protected itemRole role;

    //creating a rect object
    protected RectF collisionRect;

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    //item role
    protected enum itemRole{
        SOLID, DESTROYABLE, WIN, COLLECTABLE, ENEMY, PLAYER;
    }

    /**
     * Super constructor for all items;
     * @param context - the resource context;
     * @param xRes - x axis limit derived from resolution;
     * @param yRes - y axis limit derived from resolution;
     * @param x - location in x axis;
     * @param y - location in y axis;
     * @param bitmapName - bitmap name inside /drawable/ without the extension;
     */
    public Item(Context context, int xRes, int yRes, int x, int y, String bitmapName)
        {
        this.x = x;
        this.y = y;
        this.bitmapName = bitmapName;
        int imgID = context.getResources().getIdentifier(bitmapName, "drawable", "reading.ac.uk.androidgame");
        bitmap = BitmapFactory.decodeResource(context.getResources(), imgID);

        xSize = bitmap.getWidth();
        ySize = bitmap.getHeight();
        xMax = xRes - xSize;
        yMax = yRes - ySize;

        //initializing rect hitbox object
        collisionRect = new RectF(x, y, (x + xSize), (y + ySize));
    }

    /**
     * Method to invoke the RectF method to check whether the object which calls the method and the object sent intersect
     * @param objectRect RectF of object being tested against
     * @return return if collision occurs or not
     */
    public boolean checkCollision(RectF objectRect) {
        if (RectF.intersects(collisionRect, objectRect)) {
            return true;
        }
        return false;
    }

    /**
     * Method to check if the item is out of bounds of the screen
     */
    public void checkWall()
    {
        //check if not out of bounds
        if (x + xSize > xMax) {
            x = xMax - xSize;
        }
        if (x - xSize < 0) {
            x = 0 + xSize;
        }
        if (y + ySize > yMax) {
            y = yMax - ySize;
        }
        if (y - ySize < 0) {
            y = 0 + ySize;
        }
    }

    /**
     * Method to draw the bitmap on a given canvas
     * @param canvas canvas to draw the bitmap on
     * @param paint paint settings object
     */
    public void draw(Canvas canvas, Paint paint) {
        canvas.drawBitmap(bitmap, x, y, paint);
    }

    /**
     * Method to set the role and behaviour of an Item
     * @param input enum value of the role
     */
    public void setItemRole(itemRole input) {
        role = input;
    }

    public String getItemRole(){
        return String.valueOf(role);
    }

    /**
     * Method to return save-able string the object to allow recreation
     * @return string with information of object
     */
    @Override
    public String toString(){
        return getItemRole() +";" +x +";" +y +";" + bitmapName+";";
    }

    //getter for the rect object
    public RectF getCollisionRect() {
        return collisionRect;
    }
}
