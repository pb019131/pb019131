package reading.ac.uk.androidgame;

import android.app.AlertDialog;
import android.graphics.Point;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class Game extends AppCompatActivity {

    private GameView gView = null;
    private ConstraintLayout container = null;
    private TextView scoreTxt;
    //score display for win screen
    private TextView scoreWin = null;
    private Bundle extras;
    private String levelFile;
    private String userLevel;
    private int levelID = 1;


    /**
     * Over-ridden method to start Game activity;
     * @param savedInstanceState - bundle to pass the selection of which level to play;
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null && levelID == 1) {
            this.extras = getIntent().getExtras();
            if(extras == null) {
                finish();
            } else {
                levelFile = extras.getString("LEVEL_NAME");
                userLevel = extras.getString("LEVEL_FILE");

            }
        } else {
            levelFile = (String) savedInstanceState.getSerializable("LEVEL_NAME");
            userLevel = extras.getString("LEVEL_FILE");
        }

        //Getting display object and the screen resolution
        Display display = getWindowManager().getDefaultDisplay();
        Point resolution = new Point();
        display.getSize(resolution);
        gView = new GameView(this, resolution.x, resolution.y, this);

        //Create a layout container to add the score overlay and GameView together
        LayoutInflater mLayoutInflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View scoreView = mLayoutInflater.inflate(R.layout.activity_game, (ViewGroup) findViewById(R.id.gameStack));
        container = new ConstraintLayout(this);
        container.addView(gView);
        container.addView(scoreView);

        //get the score text object that's going to be updated
        scoreTxt = scoreView.findViewById(R.id.scoreInt);

        setContentView(container);
    }

    /**
     * Method to update the text score in the main view
     * @param score - String value of score from level;
     */
    public void setScoreText(String score) {
        final String newScore = score;
        this.runOnUiThread(new Runnable() {
            public void run() {
                scoreTxt.setText(newScore);
            }
        });
    }

    //pausing the game and thread if activity is paused
    @Override
    protected void onPause() {
        super.onPause();
        gView.pause();
    }

    //resuming the game once the activity is resumed
    @Override
    protected void onResume() {
        super.onResume();
        gView.resume();
    }

    public String getUserLevelFile() {
        return userLevel;
    }

    public String getLevelFile() {
        return levelFile;
    }


    /**Method to show a game over screen on the ui thread; Originated from gameOver in Level class;
     * This method implements a Runnable and custom AlertDialog to get the user to re-play the level;
     * **/
    public void showGameOver() {
        this.runOnUiThread(new Runnable() {
            public void run() {

                LayoutInflater layoutInflater = getLayoutInflater();

                View promptView = layoutInflater.inflate(R.layout.game_over, null);

                final AlertDialog alertD = new AlertDialog.Builder(Game.this).create();
                alertD.setCancelable(false);
                alertD.setView(promptView);

                Button restart = promptView.findViewById(R.id.continueButton);

                Button exit = promptView.findViewById(R.id.exitButton);

                restart.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        try {
                            gView.restartLevel();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        alertD.dismiss();
                    }
                });

                exit.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        finish();
                        alertD.dismiss();
                    }
                });

                alertD.setView(promptView);
                if(!isFinishing()){
                    alertD.show();
                }
            }
        });
    }

    /**Method to show a level completed screen on the ui thread; Originated from levelCompleted in Level class;
     * This method implements a Runnable and custom AlertDialog to get the user to continue playing the levels by loading the next one on button click;
     * @param score - String value of final level score to be shown on the pop-up;
     * **/
    public void showWin(String score) {
        final String assignScore = score;
        System.out.println(score);
        this.runOnUiThread(new Runnable() {
            public void run() {

                LayoutInflater layoutInflater = getLayoutInflater();
                View promptView = layoutInflater.inflate(R.layout.win, null);
                final AlertDialog alertD = new AlertDialog.Builder(Game.this).create();
                alertD.setCancelable(false);

                //setting the score
                scoreWin = promptView.findViewById(R.id.score);
                scoreWin.setText(assignScore);

                alertD.setView(promptView);

                Button continueButton = promptView.findViewById(R.id.continueButton);
                Button exit = promptView.findViewById(R.id.exitButton);

                //try to load next level but if failed restart the current one
                continueButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if(levelID < 3) {
                            levelFile = "level" + (levelID + 1);
                            levelID++;
                            try {
                                gView.nextLevel(levelFile);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else{
                            try {
                                gView.restartLevel();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        alertD.dismiss();
                    }
                });

                exit.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        finish();
                        alertD.dismiss();
                    }
                });

                alertD.setView(promptView);
                if(!isFinishing()){
                    alertD.show();
                }
            }
        });
    }
/**
    public void showPause() {
        this.runOnUiThread(new Runnable() {
            public void run() {

                LayoutInflater layoutInflater = getLayoutInflater();

                View promptView = layoutInflater.inflate(R.layout.game_over, null);

                final AlertDialog alertD = new AlertDialog.Builder(Game.this).create();
                alertD.setCancelable(false);
                alertD.setView(promptView);

                Button restart = promptView.findViewById(R.id.continueButton);

                Button exit = promptView.findViewById(R.id.exitButton);

                restart.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        System.out.println("RESTARTED");
                        recreate();
                        alertD.dismiss();
                    }
                });

                exit.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        System.out.println("EXIT");
                        finish();
                        alertD.dismiss();
                    }
                });

                alertD.setView(promptView);
                if(!isFinishing()){
                    alertD.show();
                }
            }
        });
    }
**/
}

