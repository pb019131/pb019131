package reading.ac.uk.androidgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity{

    private Button buttonPlay, buttonExit, buttonAbout, buttonScores, buttonSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //getting the menu buttons
        buttonPlay = findViewById(R.id.buttonPlay);
        buttonExit = findViewById(R.id.buttonExit);
        buttonAbout = findViewById(R.id.buttonAbout);
        buttonScores = findViewById(R.id.buttonScores);
        buttonSettings = findViewById(R.id.buttonSettings);
    }

    /**
     * Method ran on button "Play" click in the main activity; Starts a new Level Select activity;
     * @param view - view in which the button pressed is located;
     */
    public void startGame(View view) {
        startActivity(new Intent(this, LevelSelect.class));
    }

    /**
     * Unimplemented method ran on button "About" click in the main activity; Starts a new About activity;
     * @param view - view in which the button pressed is located;
     */
    public void aboutGame(View view) {
        //startActivity(new Intent(this, .class));
    }

    /**
     * Unimplemented method ran on button "Scores" click in the main activity; Starts a new Score activity;
     * @param view - view in which the button pressed is located;
     */
    public void scoresGame(View view) {
        //startActivity(new Intent(this, .class));
    }

    /**
     * Unimplemented method ran on button "Settings" click in the main activity; Starts a new Settings activity;
     * @param view - view in which the button pressed is located;
     */
    public void settingsGame(View view) {
        //startActivity(new Intent(this, .class));
    }

    /**
     * Method ran on button "Exit" click in the main activity; Closes the activity;
     * @param view - view in which the button is located
     */
    public void exitGame(View view) {
        this.finish();
    }
}
