package reading.ac.uk.androidgame;

import android.content.Context;

import static reading.ac.uk.androidgame.Item.itemRole.COLLECTABLE;

public class Collectable extends Item {

    private int scoreBonus;

    /**
     * Constructor for Collectable
     * @param context - resource context;
     * @param xRes - x axis limit derived from resolution;
     * @param yRes - y axis limit derived from resolution;
     * @param x - location in x axis;
     * @param y - location in y axis;
     * @param bitmapName - bitmap name inside /drawable/ without the extension;
     */
    public Collectable(Context context, int xRes, int yRes, int x, int y, String bitmapName) {
        super(context, xRes, yRes, x, y, bitmapName);
        setItemRole(COLLECTABLE);
        scoreBonus = 3000;
    }

    public int getScoreBonus() {
        return scoreBonus;
    }
}
